semver-checker
==============

semver-checker is a PHP utility that verifies a library's API is semver
compliant.

Rationale
---------
[Semantic Versioning](https://semver.org/) sounds great, you automatically get
the latest and greatest version of upstream code and don't have any breaking
changes. Except, it actually relies on the maintainer to identify if they've
made any breaking changes, and increment the major version accordingly.

semver-checker is an attempt to automate the process of verifying that a newer
version of the library doesn't have breaking changes. It aims to do as much as
reasonably possible by analyzing the interface of the code - but it is no
substitute for proper integration testing. Library authors may also find this
useful, as it helps identify if you have made any breaking changes.

Usage:

```
$ ./bin/semver-checker check ~/projects/monolog 1.22.1 1.23.0
semver-checker
==============
Parsing old version...
 106 [============================] 16.0 MiB
Saved to cache.
Parsing newer version...
 106 [============================] 34.0 MiB
Saved to cache.
Comparing...
318/318 [============================] 100% 40.0 MiB
Deleted items
=============
* Monolog\Formatter\GelfMessageFormatter::MAX_LENGTH was deleted

Potential breaking changes found.
```

The `upgrade` command figures out the highest semver-compatible version
available for a library, and tests to see any changes:
```
$ ./bin/semver-checker upgrade pimple/pimple 3.0.2
semver-checker
==============
Testing an upgrade from 3.0.2 to v3.2.2...
Parsing old version...
  16 [============================] 8.0 MiB
Parsing newer version...
  32 [============================] 8.0 MiB
Comparing...
80/80 [============================] 100% 8.0 MiB
No breaking changes were detected.
```

Caveats
-------
It's important to remember that not all breaking changes are created equal. The
monolog example mentioned above seems unlikely to affect any code, though it
technically is a breaking change.

On the other hand, it's possible to change code inside functions that cause
breaking changes that this tool will be unable to detect. Manual review is
always recommended.

Copyright
---------
semver-checker is Copyright (C) 2017-2018 Kunal Mehta, under the terms of the
GNU General Public License version 3, or any later version.
See COPYING for full details.
