<?php
/**
 * semver-checker - verifies a library's API is semver compliant
 * Copyright (C) 2017-2018 Kunal Mehta <legoktm@member.fsf.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Legoktm\SemverChecker;

use Legoktm\SemverChecker\Issue\IssueAdder;
use Legoktm\SemverChecker\Issue\IssueHolder;
use Legoktm\SemverChecker\Issue\SyntaxErrorIssue;
use Legoktm\SemverChecker\Parser\InheritanceResolver;
use Legoktm\SemverChecker\Parser\PrivateRemover;
use PhpParser\Error;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitor\NameResolver;
use PhpParser\Parser;
use PhpParser\ParserFactory;
use RecursiveCallbackFilterIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;

/**
 * Build out the project's interface
 */
class InterfaceBuilder implements IssueAdder {

	use IssueHolder;

	/**
	 * Bump whenever cache needs to be invalidated
	 */
	const CACHE_VERSION = 2;

	/**
	 * @var string Project location
	 */
	private $path;

	/**
	 * @var Progress
	 */
	private $progress;

	/**
	 * @var bool Whether protected functions are part of the interface
	 */
	private $includeProtected;

	/**
	 * @param string $path
	 */
	public function __construct( $path ) {
		$this->path = $path;
	}

	public function setProgress( Progress $progress ) {
		$this->progress = $progress;
	}

	/**
	 * Whether protected functions are part of the interface
	 *
	 * @param bool $include
	 */
	public function setIncludeProtected( $include ) {
		$this->includeProtected = $include;
	}

	/**
	 * @param string $path
	 *
	 * @return SplFileInfo[]
	 */
	private function listFiles( $path ) {
		return new RecursiveIteratorIterator(
			new RecursiveCallbackFilterIterator(
				new RecursiveDirectoryIterator( $path ),
				function ( SplFileInfo $current, $key, $iterator ) {
					if ( $current->isFile() && $current->getExtension() !== 'php' ) {
						// We only want *.php files.
						return false;
					}

					return true;
				}
			)
		);
	}

	public function build() {
		$files = $this->listFiles( $this->path );
		$this->progress->start( null );
		$parser = ( new ParserFactory() )->create( ParserFactory::PREFER_PHP7 );
		$info = [];
		$resolver = new InheritanceResolver();
		foreach ( $files as $file ) {
			$path = $file->getPathname();
			try {
				$info[$path] = $this->buildFile( $parser, $resolver, $path );
			} catch ( Error $e ) {
				if ( strpos( $e->getMessage(), 'Syntax error' ) === 0 ) {
					$this->addIssue( new SyntaxErrorIssue( $path ) );
				} else {
					throw $e;
				}
			}

			$this->progress->incr();
		}

		$resolver->finish();
		$this->progress->done();

		return $info;
	}

	private function buildFile( Parser $parser, InheritanceResolver $resolver, $path ) {
		$contents = file_get_contents( $path );
		$tree = $parser->parse( $contents );
		$traverser = new NodeTraverser();
		$traverser->addVisitor( new NameResolver() );
		$traverser->addVisitor( new PrivateRemover() );
		$traverser->addVisitor( $resolver );
		$tree = $traverser->traverse( $tree );

		return $tree;
	}

}
