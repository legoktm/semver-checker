<?php
/**
 * semver-checker - verifies a library's API is semver compliant
 * Copyright (C) 2017-2018 Kunal Mehta <legoktm@member.fsf.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Legoktm\SemverChecker;

use XdgBaseDir\Xdg;

class Cache {

	private $path;

	private $prefix;

	private $options;

	/**
	 * @param string $prefix Human-readable prefix
	 * @param array $options Things to vary cache on
	 */
	public function __construct( $prefix, array $options ) {
		$cacheDir = ( new Xdg() )->getHomeCacheDir();
		$this->path = "$cacheDir/semver-checker";
		if ( !is_dir( $this->path ) ) {
			mkdir( $this->path );
		}
		$this->prefix = $prefix;
		$this->options = $options + [ 'version' => InterfaceBuilder::CACHE_VERSION ];
	}

	private function fname( $refName ) {
		$sha = sha1( json_encode( $this->options + [ 'ref' => $refName ] ) );

		return "{$this->path}/{$this->prefix}-$sha.cache";
	}

	/**
	 * @param $refName
	 * @param $info
	 *
	 * @return bool Whether written to
	 */
	public function write( $refName, $info ) {
		if ( count( $info ) < 100 ) {
			// Don't bother caching less than 100 nodes
			return false;
		}
		$fname = $this->fname( $refName );
		return (bool)file_put_contents( $fname, serialize( $info ) );
	}

	public function load( $refName ) {
		$fname = $this->fname( $refName );
		if ( !file_exists( $fname ) ) {
			return false;
		}

		return unserialize( file_get_contents( $fname ) );
	}
}
