<?php
/**
 * semver-checker - verifies a library's API is semver compliant
 * Copyright (C) 2017-2018 Kunal Mehta <legoktm@member.fsf.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Legoktm\SemverChecker\Command;

use Legoktm\SemverChecker\Issue\Issue;
use Legoktm\SemverChecker\Issue\IssuePrinter;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use UnexpectedValueException;

trait FormatHelper {

	private $formats = [ 'plain', 'json' ];

	/**
	 * @var string
	 */
	private $format;

	/**
	 * @var OutputInterface
	 */
	private $output;

	public function addFormatOption() {
		$formats = implode( ', ', $this->formats );
		$this->addOption(
			'format',
			null, InputOption::VALUE_REQUIRED,
			"Output format ($formats)",
			$this->formats[0]
		);
	}

	/*
	 * Get output to use for progress output and stuff
	 */
	public function getProgressOutput() {
		if ( $this->format === 'json' ) {
			return new NullOutput();
		} else {
			return $this->output;
		}
	}

	public function initFormat( $format, OutputInterface $output ) {
		if ( !in_array( $format, $this->formats ) ) {
			throw new UnexpectedValueException( "Invalid format $format" );
		}
		$this->format = $format;
		$this->output = $output;
	}

	/**
	 * @param Issue[][] $issues
	 */
	public function outputIssues( array $issues ) {
		$printer = new IssuePrinter( $this->output );
		if ( $this->format === 'json' ) {
			$printer->json( $issues );
		} else {
			$printer->plainText( $issues );
		}
	}

}
