<?php
/**
 * semver-checker - verifies a library's API is semver compliant
 * Copyright (C) 2017-2018 Kunal Mehta <legoktm@member.fsf.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Legoktm\SemverChecker\Command;

use Legoktm\SemverChecker\Git\GitClone;
use Legoktm\SemverChecker\InterfaceComparer;
use Legoktm\SemverChecker\PackageUpgrader;
use Legoktm\SemverChecker\Packagist;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpgradeCommand extends Command {

	use FormatHelper;

	protected function configure() {
		$this->setName( 'upgrade' )
			->setDescription( 'Verifies a library\'s API is semver compliant' )
			->addArgument(
				'package',
				InputArgument::REQUIRED,
				'Package name to check'
			)->addArgument(
				'current',
				InputArgument::REQUIRED,
				'Current version to check against'
			)->addOption(
				'subdir',
				null, InputOption::VALUE_REQUIRED,
				'Subdirectory inside the repository to scan',
				'src'
			)->addOption(
				'unsafe',
				null, InputOption::VALUE_NONE,
				'Check against unsafe version upgrades'
			);
		$this->addFormatOption();
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {
		$this->initFormat( $input->getOption( 'format' ), $output );
		$pOutput = $this->getProgressOutput();
		$pOutput->writeln( [
			'semver-checker',
			'=============='
		] );

		$package = $input->getArgument( 'package' );
		$pkgInfo = ( new Packagist() )->getPackageInfo( $package );
		$constraint = $input->getOption( 'unsafe' )
			? PackageUpgrader::UNSAFE
			: PackageUpgrader::SAFE;
		$upgrader = new PackageUpgrader( $pOutput, $constraint );
		$current = $input->getArgument( 'current' );
		$repo = ( new GitClone() )->run( $pkgInfo['git'] );
		if ( !$repo->hasTag( $current ) ) {
			$current = "v$current";
			if ( !$repo->hasTag( $current ) ) {
				$output->writeln(
					"<error>Unable to map version $current to a git tag</error>"
				);
				return false;
			}
		}

		$newest = $upgrader->getNewVersion( $pkgInfo, $current );
		if ( $newest === false ) {
			return 1;
		}

		$subdir = $input->getOption( 'subdir' );
		$comparer = new InterfaceComparer( $pOutput, $repo->getPath(), $subdir );
		$issues = $comparer->compare( $current, $newest );

		$this->outputIssues( $issues );
		if ( $issues ) {
			$pOutput->writeln( 'Potential breaking changes found.' );
			return 1;
		} else {
			$pOutput->writeln( 'No breaking changes were detected.' );
		}
	}

}
