<?php
/**
 * semver-checker - verifies a library's API is semver compliant
 * Copyright (C) 2017-2018 Kunal Mehta <legoktm@member.fsf.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Legoktm\SemverChecker\Command;

use Legoktm\SemverChecker\InterfaceComparer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CheckCommand extends Command {

	use FormatHelper;

	protected function configure() {
		$this->setName( 'check' )
			->setDescription( 'Verifies a library\'s API is semver compliant' )
			->addArgument(
				'path',
				InputArgument::REQUIRED,
				'Path to repository to check'
			)->addArgument(
				'oldtag',
				InputArgument::REQUIRED,
				'Old tag to check'
			)->addArgument(
				'newtag',
				InputArgument::REQUIRED,
				'New tag to check'
			)->addOption(
				'subdir',
				null, InputOption::VALUE_REQUIRED,
				'Subdirectory inside the repository to scan',
				'src'
			);
		$this->addFormatOption();
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {
		$this->initFormat( $input->getOption( 'format' ), $output );
		$pOutput = $this->getProgressOutput();
		$pOutput->writeln( [
			'semver-checker',
			'=============='
		] );

		$path = $input->getArgument( 'path' );
		$subdir = $input->getOption( 'subdir' );
		$comparer = new InterfaceComparer( $pOutput, $path, $subdir );
		$oldTag = $input->getArgument( 'oldtag' );
		$newTag = $input->getArgument( 'newtag' );
		$issues = $comparer->compare( $oldTag, $newTag );

		$this->outputIssues( $issues );
		if ( $issues ) {
			$pOutput->writeln( 'Potential breaking changes found.' );
		} else {
			$pOutput->writeln( 'No breaking changes were detected.' );
		}
	}
}
