<?php
/**
 * semver-checker - verifies a library's API is semver compliant
 * Copyright (C) 2017-2018 Kunal Mehta <legoktm@member.fsf.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Legoktm\SemverChecker\Command;

use Legoktm\SemverChecker\Git\GitClone;
use Legoktm\SemverChecker\InterfaceComparer;
use Legoktm\SemverChecker\Issue\IssuePrinter;
use Legoktm\SemverChecker\PackageUpgrader;
use Legoktm\SemverChecker\Packagist;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpgradeLockCommand extends Command {
	protected function configure() {
		$this->setName( 'upgrade-lock' )
			->setDescription( 'Reads composer.lock, and identifies safe libraries to upgrade' )
			->addArgument(
				'lockfile',
				InputArgument::REQUIRED,
				'Location of composer.lock'
			)->addOption(
				'unsafe',
				null, InputOption::VALUE_NONE,
				'Check against unsafe version upgrades'
			);
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {
		$lockfile = $input->getArgument( 'lockfile' );
		if ( !file_exists( $lockfile ) ) {
			$output->writeln(
				"<error>$lockfile does not exist</error>"
			);
			return 1;
		}

		$json = json_decode( file_get_contents( $lockfile ), true );
		if ( !$json ) {
			$output->writeln(
				"<error>Unable to parse $lockfile</error>"
			);
			return 1;
		}

		$packages = [];
		$constraint = $input->getOption( 'unsafe' )
			? PackageUpgrader::UNSAFE
			: PackageUpgrader::SAFE;
		$upgrader = new PackageUpgrader( $output, $constraint );
		foreach ( $json['packages'] as $installed ) {
			$resp = $this->checkPackage( $output, $upgrader, $installed );
			if ( $resp ) {
				$packages[$installed['name']] = [
					$installed['version'], $resp
				];
			}
		}

		$output->writeln(
			'The following packages appear safe to upgrade:'
		);
		foreach ( $packages as $name => $versions ) {
			$output->writeln( "$name: {$versions[0]} → {$versions[1]}" );
		}
	}

	protected function checkPackage( OutputInterface $output,
		PackageUpgrader $upgrader, array $installed ) {
		$package = $installed['name'];
		$current = $installed['version'];
		$output->writeln(
			"Looking up $package..."
		);
		$pkgInfo = ( new Packagist() )->getPackageInfo( $package );
		$repo = ( new GitClone() )->run( $pkgInfo['git'] );
		if ( !$repo->hasTag( $current ) ) {
			$current = "v$current";
			if ( !$repo->hasTag( $current ) ) {
				$output->writeln(
					"<error>Unable to map version $current to a git tag</error>"
				);
				return false;
			}
		}
		$newest = $upgrader->getNewVersion( $pkgInfo, $current );
		if ( $newest === false ) {
			return false;
		}

		// TODO hardcode src
		$comparer = new InterfaceComparer( $output, $repo->getPath(), '' );
		$issues = $comparer->compare( $current, $newest );
		( new IssuePrinter( $output ) )->plainText( $issues );
		return !$issues ? $newest : false;
	}
}
