<?php
/**
 * semver-checker - verifies a library's API is semver compliant
 * Copyright (C) 2017-2018 Kunal Mehta <legoktm@member.fsf.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Legoktm\SemverChecker\Issue;

use Symfony\Component\Console\Output\OutputInterface;

class IssuePrinter {

	private $output;

	public function __construct( OutputInterface $output ) {
		$this->output = $output;
	}

	/**
	 * @param Issue[][] $issues
	 */
	public function plainText( array $issues ) {
		foreach ( $issues as $type => $typeIssues ) {
			$this->output->writeln( [
				$typeIssues[0]->desc,
				str_repeat( '=', strlen( $typeIssues[0]->desc ) )
			] );
			foreach ( $typeIssues as $issue ) {
				$this->output->writeln(
					"* {$issue->getHumanMessage()}"
				);
			}
			$this->output->writeln( '' );
		}
	}

	/**
	 * @param Issue[][] $issues
	 * @param int $flags e.g. JSON_PRETTY_PRINT
	 *
	 * @return string
	 */
	public function json( array $issues, $flags = 0 ) {
		$this->output->writeln( json_encode( $issues, $flags ) );
	}
}
