<?php
/**
 * semver-checker - verifies a library's API is semver compliant
 * Copyright (C) 2017-2018 Kunal Mehta <legoktm@member.fsf.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Legoktm\SemverChecker\Issue;

use JsonSerializable;

abstract class Issue implements JsonSerializable {

	public $desc;

	public $type;

	/**
	 * @var string Format string for messages
	 */
	public $format;

	/**
	 * @var string
	 */
	public $objectName;

	/**
	 * @var array
	 */
	public $params;

	public function __construct( $objectName, $params = [] ) {
		$this->objectName = $objectName;
		$this->params = $params;
	}

	public function getHumanMessage() {
		return vsprintf( $this->format, [ $this->objectName ] + $this->params );
	}

	public function jsonSerialize() {
		return [
			'type' => $this->type,
			'name' => $this->objectName,
			'params' => $this->params
		];
	}

}
