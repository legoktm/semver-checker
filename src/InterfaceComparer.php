<?php
/**
 * semver-checker - verifies a library's API is semver compliant
 * Copyright (C) 2017-2018 Kunal Mehta <legoktm@member.fsf.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Legoktm\SemverChecker;

use Legoktm\SemverChecker\Git\GitRepository;
use Legoktm\SemverChecker\Issue\Issue;
use Legoktm\SemverChecker\Parser\MapBuilder;
use Legoktm\SemverChecker\Parser\FunctionTracker;
use Legoktm\SemverChecker\Parser\ClassTracker;
use Legoktm\SemverChecker\Parser\ParamsComparer;
use PhpParser\NodeTraverser;
use Symfony\Component\Console\Output\OutputInterface;

class InterfaceComparer {

	/**
	 * @var OutputInterface
	 */
	private $output;

	/**
	 * @var GitRepository
	 */
	private $git;

	/**
	 * @var string
	 */
	private $path;

	/**
	 * @var string
	 */
	private $subdir;

	public function __construct( OutputInterface $output, $path, $subdir ) {
		$this->output = $output;
		$this->path = $path;
		$this->subdir = $subdir;
		$this->git = new GitRepository( $path );
	}

	public function compare( $oldTag, $newTag ) {
		$cache = new Cache(
			basename( $this->path ),
			[
				'url' => $this->git->getOriginUrl(),
				'subdir' => $this->subdir,
			]
		);
		$builder = new InterfaceBuilder(
			realpath( "{$this->path}/{$this->subdir}" )
		);
		$oldTree = $cache->load( $oldTag );
		if ( $oldTree === false ) {
			$this->git->checkout( $oldTag );
			$this->output->writeln( "Parsing old version..." );
			$builder->setProgress( new Progress( $this->output ) );
			$oldTree = $builder->build();
			if ( $this->git->isImmutable( $oldTag )
				&& !$builder->getIssues()
			) {
				if ( $cache->write( $oldTag, $oldTree ) ) {
					$this->output->writeln( 'Saved to cache.' );
				}
			}
		} else {
			$this->output->writeln( 'Loaded old version from cache!' );
		}

		$newTree = $cache->load( $newTag );
		if ( $newTree === false ) {
			$this->git->checkout( $newTag );
			$this->output->writeln( "Parsing newer version..." );
			$builder->setProgress( new Progress( $this->output ) );
			$newTree = $builder->build();
			if ( $this->git->isImmutable( $newTag )
				&& !$builder->getIssues()
			) {
				if ( $cache->write( $newTag, $newTree ) ) {
					$this->output->writeln( 'Saved to cache.' );
				}
			}
		} else {
			$this->output->writeln( 'Loaded new version from cache!' );
		}

		$this->output->writeln( "Comparing..." );
		return array_merge_recursive(
			$builder->getIssues(),
			$this->compareTrees( $oldTree, $newTree )
		);
	}

	/**
	 * @param $old
	 * @param $new
	 *
	 * @return Issue[][]
	 */
	private function compareTrees( $old, $new ) {
		$progress = new Progress( $this->output );
		// We go through old once, and new twice
		$progress->start( count( $old ) + ( count( $new ) * 2 ) );
		$firstPass = new NodeTraverser();
		$mapBuilder = new MapBuilder();
		$firstPass->addVisitor( $mapBuilder );
		foreach ( $old as $oldFile ) {
			$firstPass->traverse( $oldFile );
			$progress->incr();
		}

		$oldClassMap = $mapBuilder->getClassMap();
		$oldFuncMap = $mapBuilder->getFunctionMap();
		$mapBuilder->reset();

		foreach ( $new as $newFile ) {
			$firstPass->traverse( $newFile );
			$progress->incr();
		}

		$newClassMap = $mapBuilder->getClassMap();
		$newFuncMap = $mapBuilder->getFunctionMap();
		$secondPass = new NodeTraverser();
		$classTracker = new ClassTracker( $oldClassMap, $newClassMap );
		$functionTracker = new FunctionTracker( $oldFuncMap, $newFuncMap );
		$paramsComparer = new ParamsComparer( $classTracker, $newClassMap );
		$classTracker->setParamsComparer( $paramsComparer );
		$functionTracker->setParamsComparer( $paramsComparer );
		$secondPass->addVisitor( $classTracker );
		$secondPass->addVisitor( $functionTracker );

		foreach ( $new as $newFile ) {
			$secondPass->traverse( $newFile );
			$progress->incr();
		}

		$progress->done();
		$classTracker->checkDeletedClasses();
		$functionTracker->checkDeletedFunctions();
		return array_merge_recursive(
			$classTracker->getIssues(),
			$functionTracker->getIssues()
		);
	}
}
