<?php
/**
 * semver-checker - verifies a library's API is semver compliant
 * Copyright (C) 2017-2018 Kunal Mehta <legoktm@member.fsf.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Legoktm\SemverChecker;

use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Wrapper around symfony/console's ProgressBar
 */
class Progress {

	/**
	 * @var ProgressBar
	 */
	private $bar;

	/**
	 * @var OutputInterface
	 */
	private $output;

	/**
	 * @param OutputInterface $output
	 */
	public function __construct( OutputInterface $output ) {
		$this->output = $output;
		$this->bar = new ProgressBar( $this->output );
	}

	public function start( $count ) {
		$max = $count !== null ? '/%max%' : '';
		$percent = $count !== null ? ' %percent:3s%%' : '';
		$this->bar->setFormat( "%current%$max [%bar%]$percent %memory:6s%" );
		$this->bar->start( $count );
	}

	public function incr() {
		$this->bar->advance();
	}

	public function done() {
		$this->bar->finish();
		$this->output->writeLn( '' );
	}

}
