<?php
/**
 * semver-checker - verifies a library's API is semver compliant
 * Copyright (C) 2017-2018 Kunal Mehta <legoktm@member.fsf.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Legoktm\SemverChecker;

use Requests;

class Packagist {

	/**
	 * @param string $name
	 *
	 * @return array
	 */
	public function getPackageInfo( $name ) {
		$req = Requests::get( "https://packagist.org/packages/$name.json" );
		$req->throw_for_status();

		$data = json_decode( $req->body, true )['package'];
		return [
			'git' => $data['repository'],
			'versions' => array_filter(
				array_keys( $data['versions'] ),
				function ( $version ) {
					return strpos( $version, 'dev' ) === false;
				}
			),
		];
	}
}
