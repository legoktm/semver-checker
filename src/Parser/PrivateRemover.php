<?php
/**
 * semver-checker - verifies a library's API is semver compliant
 * Copyright (C) 2017-2018 Kunal Mehta <legoktm@member.fsf.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Legoktm\SemverChecker\Parser;

use PhpParser\Node;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitorAbstract;

/**
 * Drop private things from the AST to shrink the cache
 * size as much as possible
 */
class PrivateRemover extends NodeVisitorAbstract {

	public function leaveNode( Node $node ) {
		if ( $node instanceof Node\Stmt\ClassMethod ) {
			if ( $node->isPrivate() ) {
				return NodeTraverser::REMOVE_NODE;
			}
		} elseif ( $node instanceof Node\Stmt\ClassConst ) {
			if ( $node->isPrivate() ) {
				return NodeTraverser::REMOVE_NODE;
			}
		} elseif ( $node instanceof Node\Stmt\Property ) {
			if ( $node->isPrivate() ) {
				return NodeTraverser::REMOVE_NODE;
			}
		}
	}
}
