<?php
/**
 * semver-checker - verifies a library's API is semver compliant
 * Copyright (C) 2017-2018 Kunal Mehta <legoktm@member.fsf.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Legoktm\SemverChecker\Parser;

use Legoktm\SemverChecker\Issue\DeletedIssue;
use Legoktm\SemverChecker\Issue\IssueAdder;
use Legoktm\SemverChecker\Issue\IssueHolder;
use PhpParser\Node;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitorAbstract;

class FunctionTracker extends NodeVisitorAbstract implements IssueAdder {

	use IssueHolder;

	/**
	 * @var ParamsComparer
	 */
	private $paramsComparer;

	/**
	 * @var Node\Stmt\Function_[]
	 */
	private $oldByName = [];
	/**
	 * @var Node\Stmt\Function_[]
	 */
	private $newByName = [];

	public function __construct( $oldMap, $newMap ) {
		$this->oldByName = $oldMap;
		$this->newByName = $newMap;
	}

	public function setParamsComparer( ParamsComparer $comparer ) {
		$this->paramsComparer = $comparer;
	}

	public function enterNode( Node $node ) {
		if ( $node instanceof Node\Stmt\Function_ ) {
			if ( isset( $this->oldByName[$node->name] ) ) {
				$oldNode = $this->oldByName[$node->name];
				// Compare params
				$this->paramsComparer->compare(
					$node->name,
					$oldNode->getParams(),
					$node->getParams()
				);
			}

			return NodeTraverser::DONT_TRAVERSE_CHILDREN;
		}
	}

	public function checkDeletedFunctions() {
		$funcs = array_diff(
			array_keys( $this->oldByName ),
			array_keys( $this->newByName )
		);
		foreach ( $funcs as $func ) {
			$this->addIssue( new DeletedIssue( $func . '()' ) );
		}
	}

}
