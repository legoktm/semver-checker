<?php
/**
 * semver-checker - verifies a library's API is semver compliant
 * Copyright (C) 2017-2018 Kunal Mehta <legoktm@member.fsf.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Legoktm\SemverChecker\Parser;

use PhpParser\Node;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitorAbstract;

class MapBuilder extends NodeVisitorAbstract {

	/**
	 * @var Node\Stmt\ClassLike[]
	 */
	private $classMap = [];

	/**
	 * @var Node\Stmt\Function_[]
	 */
	private $funcMap = [];

	public function enterNode( Node $node ) {
		if ( $node instanceof Node\Stmt\ClassLike ) {
			$this->classMap[(string)$node->namespacedName] = $node;
			return NodeTraverser::DONT_TRAVERSE_CHILDREN;
		} elseif ( $node instanceof Node\Stmt\Function_ ) {
			$this->funcMap[(string)$node->namespacedName] = $node;
		}
	}

	/**
	 * @return Node\Stmt\ClassLike[]
	 */
	public function getClassMap() {
		return $this->classMap;
	}

	/**
	 * @return Node\Stmt\Function_[]
	 */
	public function getFunctionMap() {
		return $this->funcMap;
	}

	public function reset() {
		$this->classMap = [];
		$this->funcMap = [];
	}

}
