<?php
/**
 * semver-checker - verifies a library's API is semver compliant
 * Copyright (C) 2017-2018 Kunal Mehta <legoktm@member.fsf.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Legoktm\SemverChecker\Parser;

use Legoktm\SemverChecker\Issue\Issue;
use Legoktm\SemverChecker\Issue\IssueAdder;
use Legoktm\SemverChecker\Issue\NewMandatoryParameterIssue;
use Legoktm\SemverChecker\Issue\TypeHintChangeIssue;
use PhpParser\Node\Param;
use PhpParser\Node\Stmt\ClassLike;

/**
 * Compare two sets of parameters
 */
class ParamsComparer implements IssueAdder {

	/**
	 * @var IssueAdder
	 */
	private $issueAdder;

	/**
	 * @var ClassLike[]
	 */
	private $newClassMap;

	public function __construct( IssueAdder $issueAdder, array $newClassMap ) {
		$this->issueAdder = $issueAdder;
		$this->newClassMap = $newClassMap;
	}

	public function addIssue( Issue $issue ) {
		$this->issueAdder->addIssue( $issue );
	}

	private function isHintCompatible( $newType, $oldType ) {
		$newType = (string)$newType;
		$oldType = (string)$oldType;

		if ( $newType === $oldType ) {
			return true;
		}

		// builtins like Exception -> Throwable?
		if ( is_a( $oldType, $newType, true ) ) {
			return true;
		}

		if ( !isset( $this->newClassMap[$oldType] ) ) {
			return false;
		}
		$oldClassNode = $parentNode = $this->newClassMap[$oldType];
		while ( $parentNode
			&& ( $parentNode->extends || $parentNode->implements )
		) {
			foreach ( $parentNode->implements as $implement ) {
				if ( $newType === (string)$implement ) {
					// If the old type implements the new type
					// then it's a safe change
					return true;
				}
			}

			if ( $newType === (string)$parentNode->extends ) {
				// If the old type extends the new type, then
				// it's a safe change.
				return true;
			}

			if ( isset( $parentNode->extendsNode ) ) {
				$parentNode = $parentNode->extendsNode;
			} else {
				$parentNode = false;
			}
		}

		return false;
	}

	/**
	 * @param string $methodName
	 * @param Param[] $oldParams
	 * @param Param[] $newParams
	 *
	 */
	public function compare( $methodName, array $oldParams, array $newParams ) {
		// New parameters with no default = problem
		// New type-hints? Not always a problem depending on documentation
		// Change of type-hints? Need to check inheritance
		$max = max( count( $oldParams ), count( $newParams ) );
		for ( $i = 0; $i < $max; $i++ ) {
			$oldParam = isset( $oldParams[$i] ) ? $oldParams[$i] : null;
			$newParam = isset( $newParams[$i] ) ? $newParams[$i] : null;
			if ( $oldParam && !$newParam ) {
				// Parameter was removed, that's OK!
				continue;
			}
			if ( $newParam && !$oldParam ) {
				if ( !$newParam->default ) {
					// New parameter that doesn't have a default, uhoh.
					$this->addIssue( new NewMandatoryParameterIssue( $methodName, [
						'param' => $newParam->name
					] ) );
				}

				continue;
			}

			// Both new and old have parameters at this point
			// Type hints: both had them, and now they're incompatible.
			if ( $newParam->type && $oldParam->type
				&& !$this->isHintCompatible( $newParam->type, $oldParam->type )
			) {
				$this->addIssue( new TypeHintChangeIssue( $methodName, [
					'param' => $newParam->name,
					'old' => (string)$oldParam->type,
					'new' => (string)$newParam->type,
				] ) );
			}
		}
	}

}
