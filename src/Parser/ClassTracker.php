<?php
/**
 * semver-checker - verifies a library's API is semver compliant
 * Copyright (C) 2017-2018 Kunal Mehta <legoktm@member.fsf.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Legoktm\SemverChecker\Parser;

use Legoktm\SemverChecker\Issue\DeletedIssue;
use Legoktm\SemverChecker\Issue\IssueAdder;
use Legoktm\SemverChecker\Issue\IssueHolder;
use Legoktm\SemverChecker\Issue\MadeAbstractIssue;
use Legoktm\SemverChecker\Issue\TypeChangeIssue;
use Legoktm\SemverChecker\Issue\VisibilityChangeIssue;
use PhpParser\Node;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitorAbstract;

/**
 * Tracks classes and prepares a full diff between two
 * versions of the same class
 */
class ClassTracker extends NodeVisitorAbstract implements IssueAdder {

	use IssueHolder;

	/**
	 * @var ParamsComparer
	 */
	private $paramsComparer;

	/**
	 * @var Node\Stmt\ClassLike[]
	 */
	private $oldByName = [];

	/**
	 * @var Node\Stmt\ClassLike[]
	 */
	private $newByName = [];

	public function __construct( $oldMap, $newMap ) {
		$this->oldByName = $oldMap;
		$this->newByName = $newMap;
	}

	public function setParamsComparer( ParamsComparer $comparer ) {
		$this->paramsComparer = $comparer;
	}

	public function enterNode( Node $node ) {
		if ( $node instanceof Node\Stmt\ClassLike ) {
			$name = $node->namespacedName->toString();
			if ( isset( $this->oldByName[$name] ) ) {
				$oldNode = $this->oldByName[$name];
				if ( get_class( $oldNode ) !== get_class( $node ) ) {
					// Type-mismatch
					$this->addIssue( new TypeChangeIssue( $name, [
						'old' => $oldNode->getType(),
						'new' => $node->getType(),
					] ) );
				}

				// Check methods & properties when we can diff against old
				$this->handleMethods( $oldNode, $node );
				$this->handleProperties( $oldNode, $node );
				$this->handleConstants( $oldNode, $node );
			}

			return NodeTraverser::DONT_TRAVERSE_CHILDREN;
		}
	}

	/**
	 * @param Node\Stmt\ClassLike $node
	 *
	 * @return Node\Stmt\ClassMethod[]
	 */
	private function getMethodsByName( Node\Stmt\ClassLike $node ) {
		$methods = [];
		foreach ( $node->getMethods() as $method ) {
			$methods[$method->name] = $method;
		}

		return $methods;
	}

	/**
	 * @param Node\Stmt\ClassLike $node
	 *
	 * @return Node\Stmt\Property[]
	 */
	private function getPropertiesByName( Node\Stmt\ClassLike $node ) {
		$properties = [];
		foreach ( $node->stmts as $stmt ) {
			if ( $stmt instanceof Node\Stmt\Property ) {
				$properties[$stmt->props[0]->name] = $stmt;
			}
		}

		return $properties;
	}

	/**
	 * @param Node\Stmt\ClassLike $node
	 *
	 * @return Node\Stmt\ClassConst[]
	 */
	private function getConstantsByName( Node\Stmt\ClassLike $node ) {
		$constants = [];
		foreach ( $node->stmts as $stmt ) {
			if ( $stmt instanceof Node\Stmt\ClassConst ) {
				$constants[$stmt->consts[0]->name] = $stmt;
			}
		}

		return $constants;
	}

	private function visibilityToString( $node ) {
		if ( $node->isPublic() ) {
			return 'public';
		} elseif ( $node->isProtected() ) {
			return 'protected';
		} else {
			return 'private';
		}
	}

	/**
	 * @param Node\Stmt\ClassLike $oldClassNode
	 * @param Node\Stmt\ClassLike $newClassNode
	 */
	private function handleProperties( $oldClassNode, $newClassNode ) {
		$newProperties = $this->getPropertiesByName( $newClassNode );
		$oldProperties = $this->getPropertiesByName( $oldClassNode );
		foreach ( $newProperties as $name => $newProperty ) {
			if ( !isset( $oldProperties[$name] ) ) {
				// Nothing to diff against
				continue;
			}

			$oldProperty = $oldProperties[$name];
			$visibility = $this->isVisibilitySane( $oldProperty, $newProperty );
			if ( !$visibility ) {
				$this->addIssue( new VisibilityChangeIssue(
					$this->fullName( $newClassNode, "\$$name" ),
					[
						'old' => $this->visibilityToString( $oldProperty ),
						'new' => $this->visibilityToString( $newProperty )
					]
				) );
			}
		}

		$missing = array_diff(
			array_keys( $oldProperties ),
			array_keys( $newProperties )
		);

		foreach ( $missing as $missingName ) {
			$this->addIssue( new DeletedIssue(
				$this->fullName( $oldClassNode, "\$$missingName" )
			) );
		}
	}

	/**
	 * @param Node\Stmt\ClassLike $oldClassNode
	 * @param Node\Stmt\ClassLike $newClassNode
	 */
	private function handleMethods( $oldClassNode, $newClassNode ) {
		$newMethods = $this->getMethodsByName( $newClassNode );
		$oldMethods = $this->getMethodsByName( $oldClassNode );
		foreach ( $newMethods as $name => $newMethod ) {
			if ( !isset( $oldMethods[$name] ) ) {
				// Nothing to diff against
				continue;
			}
			$oldMethod = $oldMethods[$name];
			$fullName = $this->fullName( $newClassNode, $newMethod );

			$visibility = $this->isVisibilitySane( $oldMethod, $newMethod );
			if ( !$visibility ) {
				$this->addIssue( new VisibilityChangeIssue( $fullName, [
					'old' => $this->visibilityToString( $oldMethod ),
					'new' => $this->visibilityToString( $newMethod )
				] ) );
				break;
			}

			if ( $newMethod->isAbstract() && !$oldMethod->isAbstract() ) {
				$this->addIssue( new MadeAbstractIssue( $fullName ) );
			}

			$this->paramsComparer->compare(
				$fullName,
				$oldMethod->getParams(),
				$newMethod->getParams()
			);
		}

		$missing = array_diff(
			array_keys( $oldMethods ),
			array_keys( $newMethods )
		);

		$oldClassName = $oldClassNode->namespacedName->toString();
		foreach ( $missing as $missingName ) {
			if ( $missingName === $oldClassName
				&& isset( $newMethods['__construct'] )
			) {
				// If the old method was the same as the class name,
				// it was a PHP 4 style constructor. As long as __construct
				// exists now, that's fine.
				continue;
			}
			$this->addIssue( new DeletedIssue(
				$this->fullName( $oldClassNode, $missingName )
			) );
		}
	}

	/**
	 * @param Node\Stmt\ClassLike $oldClassNode
	 * @param Node\Stmt\ClassLike $newClassNode
	 */
	private function handleConstants( $oldClassNode, $newClassNode ) {
		$newConstants = $this->getConstantsByName( $newClassNode );
		$oldConstants = $this->getConstantsByName( $oldClassNode );

		$missing = array_diff(
			array_keys( $oldConstants ),
			array_keys( $newConstants )
		);

		foreach ( $missing as $missingName ) {
			$this->addIssue( new DeletedIssue(
				$this->fullName( $oldClassNode, $missingName )
			) );
		}
	}

	/**
	 * @param Node\Stmt\ClassLike $classNode
	 * @param Node\Stmt\ClassMethod|string $methodNode
	 * @return string
	 */
	private function fullName( Node\Stmt\ClassLike $classNode, $methodNode ) {
		if ( $methodNode instanceof Node\Stmt\ClassMethod ) {
			$methodName = $methodNode->name;
		} else {
			$methodName = $methodNode;
		}
		return $classNode->namespacedName->toString() . '::' . $methodName;
	}

	/**
	 * Visiblity changes are sane if functions become more
	 * visible, not less
	 *
	 * @param Node\Stmt\ClassMethod|Node\Stmt\Property $oldMethod
	 * @param Node\Stmt\ClassMethod|Node\Stmt\Property $newMethod
	 * @return bool
	 */
	private function isVisibilitySane( $oldMethod, $newMethod ) {
		if ( $oldMethod->isPublic() ) {
			return $newMethod->isPublic();
		}

		if ( $oldMethod->isProtected() ) {
			return $newMethod->isPublic() || $newMethod->isProtected();
		}

		// $oldMethod is private, so any visibility on new is OK.
		return true;
	}

	public function checkDeletedClasses() {
		$deleted = array_diff(
			array_keys( $this->oldByName ),
			array_keys( $this->newByName )
		);
		foreach ( $deleted as $class ) {
			$this->addIssue( new DeletedIssue( $class ) );
		}
	}
}
