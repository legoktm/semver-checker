<?php
/**
 * semver-checker - verifies a library's API is semver compliant
 * Copyright (C) 2017-2018 Kunal Mehta <legoktm@member.fsf.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Legoktm\SemverChecker\Git;

use InvalidArgumentException;

use RuntimeException;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Wrapper around a git repository
 */
class GitRepository {

	private $path;

	/**
	 * @param string $path Path to git repository
	 *
	 * @throws InvalidArgumentException
	 */
	public function __construct( $path ) {
		$dotGit = realpath( "$path/.git" );
		if ( !is_dir( $dotGit ) && !is_file( $dotGit ) ) {
			throw new InvalidArgumentException( "$path does not have a .git directory/file" );
		}
		$this->path = $path;
	}

	public function getPath() {
		return $this->path;
	}

	/**
	 * Execute a git command
	 *
	 * @param string $command Without "git" prefix
	 *
	 * @return string
	 */
	private function exec( $command ) {
		$process = new Process( "git $command" );
		$process->setWorkingDirectory( $this->path );
		$process->mustRun();

		return $process->getOutput();
	}

	/**
	 * Checkout a ref
	 *
	 * @param string $ref
	 */
	public function checkout( $ref ) {
		$this->exec( "checkout $ref" );
	}

	public function pull() {
		$this->exec( 'pull' );
	}

	public function hasTag( $tagName ) {
		// Cheating :p
		return $this->isImmutable( $tagName );
	}

	/**
	 * @param string $refName
	 *
	 * @return bool
	 */
	public function isImmutable( $refName ) {
		if ( (bool)preg_match( '/^[0-9A-F]{40}$/i', $refName ) ) {
			// SHA1's are immutable
			return true;
		}
		try {
			// Command exists with status code 1 if not a tag
			$this->exec( "show-ref -q --verify refs/tags/$refName" );
			return true;
		} catch ( ProcessFailedException $e ) {
			return false;
		}
	}

	/**
	 * git remote get-url is too new
	 *
	 * @return string
	 */
	public function getOriginUrl() {
		$out = $this->exec( 'remote show origin' );
		$found = preg_match( '/Fetch URL: (.*)/', $out, $matches );
		if ( $found ) {
			return $matches[1];
		} else {
			throw new RuntimeException(
				"Unable to determine git remote URL in {$this->path}" );
		}
	}
}
