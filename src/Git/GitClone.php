<?php
/**
 * semver-checker - verifies a library's API is semver compliant
 * Copyright (C) 2017-2018 Kunal Mehta <legoktm@member.fsf.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Legoktm\SemverChecker\Git;

use Symfony\Component\Process\Process;
use XdgBaseDir\Xdg;

class GitClone {
	public function __construct() {
		$cacheDir = ( new Xdg() )->getHomeCacheDir();
		$this->path = "$cacheDir/semver-checker/clones";
		if ( !is_dir( $this->path ) ) {
			mkdir( $this->path, 0777, true );
		}
	}

	/**
	 * @param string $url
	 *
	 * @return GitRepository
	 */
	public function run( $url ) {
		$dir = $this->path . '/' . basename( $url ) . sha1( $url );
		$pull = false;
		if ( !is_dir( $dir ) ) {
			$process = new Process( "git clone $url $dir" );
			$process->setWorkingDirectory( $this->path );
			$process->mustRun();
		} else {
			$pull = true;
		}

		$repo = new GitRepository( $dir );
		if ( $pull ) {
			$repo->checkout( 'master' );
			$repo->pull();
		}

		return $repo;
	}

}
